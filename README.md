# Player Ranking

This project is about a live coding question in [Testdome]() that i cannot finish in the time proposed.

## The question

We have a League in wich players accumulate scores in games.

First we register all the players.
Then we add the score reached in each game for all the players.

The rule for ranking is:

1. Higher score is the first;
2. If score are equal, then the player with fewer games played are best;
3. If the number of games also are equal, so the last registered player wins;

A method receiving a rank number must return the player that occupied the position based on the rules above.

> The only thing concrete i have is the start code of the class.  
> All the rules are in my mind, but the changes are good they are right.

## Getting the code

Just clone the repo and run:

```sh
$ composer install
```

## Testing the code

I'm using [PHPUnit](), so to run all the tests you can run this command:

```sh
$ composer exec -v phpunit LeagueTableTest.php
```
