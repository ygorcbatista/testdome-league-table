<?php

// Don't using namespace for brevity
require 'LeagueTable.php';

use PHPUnit\Framework\TestCase;

class LeagueTableTest extends TestCase
{
    public function testRankByScore()
    {
        $leagueTable = new LeagueTable(['Mike', 'Chris', 'Arnold']);

        $leagueTable->recordResult('Mike', 1);
        $leagueTable->recordResult('Arnold', 2);
        $leagueTable->recordResult('Chris', 3);

        $first = $leagueTable->playerRank(1);
        $second = $leagueTable->playerRank(2);
        $third = $leagueTable->playerRank(3);

        $this->assertEquals('Chris', $first);
        $this->assertEquals('Arnold', $second);
        $this->assertEquals('Mike', $third);
    }

    public function testRankByGamesPlayed()
    {
        $leagueTable = new LeagueTable(['Mike', 'Chris', 'Arnold']);

        $leagueTable->recordResult('Mike', 10);
        $leagueTable->recordResult('Arnold', 22);
        $leagueTable->recordResult('Chris', 21);
        $leagueTable->recordResult('Chris', 1);

        $first = $leagueTable->playerRank(1);
        $second = $leagueTable->playerRank(2);
        $third = $leagueTable->playerRank(3);

        $this->assertEquals('Arnold', $first);
        $this->assertEquals('Chris', $second);
        $this->assertEquals('Mike', $third);
    }

    public function testRankByIndex()
    {
        $leagueTable = new LeagueTable(['Mike', 'Chris', 'Arnold']);

        $leagueTable->recordResult('Mike', 14);
        $leagueTable->recordResult('Arnold', 14);
        $leagueTable->recordResult('Chris', 14);

        $first = $leagueTable->playerRank(1);
        $second = $leagueTable->playerRank(2);
        $third = $leagueTable->playerRank(3);

        $this->assertEquals('Arnold', $first);
        $this->assertEquals('Chris', $second);
        $this->assertEquals('Mike', $third);
    }

    public function testRankByAll()
    {
        $leagueTable = new LeagueTable(['Mike', 'Chris', 'Arnold']);

        $leagueTable->recordResult('Mike', 2);
        $leagueTable->recordResult('Mike', 3);
        $leagueTable->recordResult('Arnold', 5);
        $leagueTable->recordResult('Chris', 5);

        $first = $leagueTable->playerRank(1);
        $second = $leagueTable->playerRank(2);
        $third = $leagueTable->playerRank(3);

        $this->assertEquals('Arnold', $first);
        $this->assertEquals('Chris', $second);
        $this->assertEquals('Mike', $third);
    }
}