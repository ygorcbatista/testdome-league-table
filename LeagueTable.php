<?php

class LeagueTable
{
    public function __construct($players)
    {
        $this->standings = array();
        foreach ($players as $index => $p) {
            $this->standings[$p] = array(
                'index' => $index,
                'games_played' => 0,
                'score' => 0
            );
        }
    }

    public function recordResult($player, $score)
    {
        $this->standings[$player]['games_played']++;
        $this->standings[$player]['score'] += $score;
    }

    public function playerRank($rank)
    {
        // Add player name in array value
        $ranking = [];
        foreach ($this->standings as $playerName => $playerInfo) {
            $playerInfo['name'] = $playerName;
            $ranking[] = $playerInfo;
        }

        // Ordering players ascending, considering all rules
        usort($ranking, function ($playerA, $playerB) {

            if ($playerA['score'] !== $playerB['score']) {
                return ($playerA['score'] < $playerB['score']) ? -1 : +1;
            }

            if ($playerA['games_played'] !== $playerB['games_played']) {
                return ($playerA['games_played'] > $playerB['games_played']) ? -1 : +1;
            }

            return ($playerA['index'] < $playerB['index']) ? -1 : +1;

        });

        // Reverse the order, ranking must be descending
        $ranking = array_reverse($ranking);
               
        // The ranking array is 0-based
        $rank -= 1;

        return $ranking[$rank]['name'];
    }
}
